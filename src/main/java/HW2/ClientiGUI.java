package HW2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.awt.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class ClientiGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel panel = new JPanel();
	public static int timpSosire;
	public static int timpProcesare;
	public static String text = new String();
	public static int numarClienti;
	public static int numarCozi;
	public static String dataSDF;
	public static int intervalSimulare;
	public static int timpCurent = 0;
	public static boolean tr = true;
	
	private JLabel numarClientiLabel = new JLabel("Numar de clienti: ");
	private JLabel numarCoziLabel = new JLabel("Numar de cozi: ");
	private JLabel timpSosireMinimLabel = new JLabel("Timp de sosire minim: ");
	private JLabel timpSosireMaximLabel = new JLabel("Timp de sosire maxim: ");
	private JLabel timpProcesareMinimLabel = new JLabel("Timp de procesare minim: ");
	private JLabel timpProcesareMaximLabel = new JLabel("Timp de procesare maxim: ");
	private JLabel intervalSimulareLabel = new JLabel("Interval de simulare: ");
	private JLabel mediuAsteptareLabel = new JLabel("Timp mediu de asteptare");
	private JLabel medieAsteptareLabel = new JLabel("Coada 0  |  Coada 1  |  Coada 2");
	private JLabel peakHourLabel = new JLabel("Ora curenta: ");
	private JLabel loggerLabel = new JLabel("Log");
	private JLabel timerLabel = new JLabel("Evolutia cozilor");

	private JTextField numarClientiText = new JTextField();
	private JTextField numarCoziText = new JTextField();
	private JTextField timpSosireMinimText = new JTextField();
	private JTextField timpSosireMaximText = new JTextField();
	private JTextField timpProcesareMinimText = new JTextField();
	private JTextField timpProcesareMaximText = new JTextField();
	private JTextField intervalSimulareText = new JTextField();
	private JTextField medieAsteptareText = new JTextField();
	private JTextField peakHourText = new JTextField();

	private JTextArea loggerText = new JTextArea();
	private JTextArea timerText = new JTextArea();
	private JScrollPane scrollLoggerText = new JScrollPane(loggerText);
	private JScrollPane scrollTimerText = new JScrollPane(timerText);

	private JButton simulareButton = new JButton("Simulare");

	public ClientiGUI() {
		initializareEcran();
		initializareComponente();
		adaugareComponente();
		adaugareListener();
	}

	public void initializareEcran() {
		setTitle("Tema 2 - Simulare de cozi");
		setSize(new Dimension(1205, 505));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
	}

	public void initializareComponente() {
		panel.setLayout(null);
		numarClientiLabel.setBounds(25, 25, 150, 30);
		numarClientiLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numarClientiText.setBounds(175, 25, 50, 30);

		numarCoziLabel.setBounds(25, 75, 150, 30);
		numarCoziLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numarCoziText.setBounds(175, 75, 50, 30);

		timpSosireMinimLabel.setBounds(25, 125, 150, 30);
		timpSosireMinimLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		timpSosireMinimText.setBounds(175, 125, 50, 30);

		timpSosireMaximLabel.setBounds(25, 175, 150, 30);
		timpSosireMaximLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		timpSosireMaximText.setBounds(175, 175, 50, 30);

		timpProcesareMinimLabel.setBounds(20, 225, 155, 30);
		timpProcesareMinimLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		timpProcesareMinimText.setBounds(175, 225, 50, 30);

		timpProcesareMaximLabel.setBounds(20, 275, 155, 30);
		timpProcesareMaximLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		timpProcesareMaximText.setBounds(175, 275, 50, 30);

		intervalSimulareLabel.setBounds(25, 325, 150, 30);
		intervalSimulareLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		intervalSimulareText.setBounds(175, 325, 50, 30);

		peakHourLabel.setBounds(25, 375, 100, 30);
		peakHourLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		peakHourText.setBounds(125, 375, 100, 30);
		peakHourText.setHorizontalAlignment(SwingConstants.CENTER);

		peakHourText.setEditable(false);
		simulareButton.setBounds(25, 425, 200, 30);

		timerText.setEditable(false);
		loggerText.setEditable(false);
		medieAsteptareText.setEditable(false);

		scrollTimerText.setBounds(250, 25, 300, 380);
		scrollLoggerText.setBounds(575, 25, 400, 380);

		loggerLabel.setBounds(575, 425, 400, 30);
		loggerLabel.setHorizontalAlignment(SwingConstants.CENTER);

		timerLabel.setBounds(250, 425, 300, 30);
		timerLabel.setHorizontalAlignment(SwingConstants.CENTER);

		mediuAsteptareLabel.setBounds(1000, 25, 175, 30);
		mediuAsteptareLabel.setHorizontalAlignment(SwingConstants.CENTER);
		medieAsteptareLabel.setBounds(1000, 50, 175, 30);
		medieAsteptareLabel.setHorizontalAlignment(SwingConstants.CENTER);
		medieAsteptareText.setBounds(1000, 75, 175, 30);
		medieAsteptareText.setHorizontalAlignment(SwingConstants.CENTER);

		
	}

	public void adaugareComponente() {
		panel.add(numarClientiLabel);
		panel.add(numarClientiText);
		panel.add(numarCoziLabel);
		panel.add(numarCoziText);
		panel.add(timpSosireMinimLabel);
		panel.add(timpSosireMinimText);
		panel.add(timpSosireMaximLabel);
		panel.add(timpSosireMaximText);
		panel.add(timpProcesareMinimLabel);
		panel.add(timpProcesareMinimText);
		panel.add(timpProcesareMaximLabel);
		panel.add(timpProcesareMaximText);
		panel.add(intervalSimulareLabel);
		panel.add(intervalSimulareText);
		panel.add(peakHourLabel);
		panel.add(peakHourText);
		panel.add(simulareButton);
		panel.add(scrollTimerText);
		panel.add(scrollLoggerText);
		panel.add(loggerLabel);
		panel.add(timerLabel);
		panel.add(mediuAsteptareLabel);
		panel.add(medieAsteptareText);
		panel.add(medieAsteptareLabel);
		
	}
	static String textClienti = "Clienti:\n";
	static String textCozi = "Logger:\n";
	public void adaugareListener() {

		simulareButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				numarCozi = Integer.parseInt(numarCoziText.getText());
				numarClienti = Integer.parseInt(numarClientiText.getText());
				int timpSosireMinim = Integer.parseInt(timpSosireMinimText.getText());
				int timpSosireMaxim = Integer.parseInt(timpSosireMaximText.getText());
				int timpProcesareMinim = Integer.parseInt(timpProcesareMinimText.getText());
				int timpProcesareMaxim = Integer.parseInt(timpProcesareMaximText.getText());
				intervalSimulare = Integer.parseInt(intervalSimulareText.getText());
				Coada coada[] = new Coada[numarClienti];
				for(int i=0; i<numarClienti; i++){
					coada[ i ] = new Coada("coada "+ i);
					coada[ i ].start();
				} 
				Schedule scheduler = new Schedule(numarCozi , coada, "scheduler", numarClienti);
				scheduler.start();

				Thread thread = new Thread(new Runnable() {

					@Override
					public void run() {
						while(tr) {
							timpSosire = randomTimp(timpSosireMinim, timpSosireMaxim);
							timpProcesare = randomTimp(timpProcesareMinim, timpProcesareMaxim);
							Calendar cal = Calendar.getInstance();
							SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
							dataSDF = sdf.format(new Date());
							peakHourText.setText( sdf.format(cal.getTime()) );
							timpCurent++;
							if(timpCurent > intervalSimulare)
								tr = false;							
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							timerText.setText("");
							for(int i = 0; i < numarCozi; i++) {
								timerText.append("Coada " + Integer.toString(i) + ": "); 
								for(Client clientIntermediar : coada[i].getClienti()) {
									timerText.append( clientIntermediar.getID()+" ");
								}
								timerText.append("\n");
								loggerText.setText(text);
								medieAsteptareText.setText(Schedule.medieAsteptare);
							}
						}

					}
				});
				thread.start();
			}
		});
	}


	private static int randomTimp (int timpMinim, int timpMaxim)
	{
		int x=0;
		Random random = new Random();
		x = random.nextInt(timpMaxim - timpMinim + 1) + timpMinim;
		return x;
	}

	public static void main(String[] args) {
		new ClientiGUI();
	}

}