package HW2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Coada extends Thread {
	private BlockingQueue<Client> clienti = new ArrayBlockingQueue<Client>(ClientiGUI.numarClienti);
	public int asteptare;
	public static int procesare;
	public static int sosire;
	public String id;
	public Coada(String name) {
		setName(name);
	}

	@Override
	public void run() {
		try {
			while (true) {
				sleep((int) Math.random() * 1000);
				stergeClient();
			}
		} catch (InterruptedException e) {
			System.out.println("Intrerupere");
			System.out.println(e.toString());
		}
	}

	public void adaugaClient(Client client) throws InterruptedException {
		clienti.add(client);
	}

	public void stergeClient() throws InterruptedException {

		while (clienti.size() != 0) {
			Client client = clienti.peek();
			procesare = Math.abs(client.getTimpProcesare());
			sosire = Math.abs(client.getTimpSosire());
			asteptare = Math.abs(sosire + procesare);
			sleep(procesare * 1000);
			clienti.remove();
			ClientiGUI.text += (ClientiGUI.dataSDF + " Clientul " + Integer.toString(client.getID())
			+ " a fost deservit la " + getName() + " dupa " + Integer.toString(client.getTimpProcesare())
			+ "s. \n");
		}
	}

	public int lungimeCoada() throws InterruptedException {
		int size = clienti.size();
		return size;
	}

	public BlockingQueue<Client> getClienti() {
		return clienti;
	}

	public void setClienti(BlockingQueue<Client> clienti) {
		this.clienti = clienti;
	}

}
