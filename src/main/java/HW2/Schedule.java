package HW2;

public class Schedule extends Thread{
	private Coada coada[];
	public static double AVRG[] = {0, 0, 0};
	private int numarCozi;
	static int ID;
	private int numarClienti;
	private static int x[] = {1, 1, 1};
	public static String medieAsteptare = new String();

	public Schedule(int numarCozi, Coada coada[], String nume, int numarClienti){
		setName (getName());
		this.numarCozi = numarCozi;
		this.coada = new Coada[numarCozi];
		this.numarClienti = numarClienti;
		for(int i=0; i< numarCozi; i++){
			this.coada[i] = coada[i] ; 	
		}
	}

	private int min_index (){
		int index = 0;
		try{
			int min = coada[0].lungimeCoada();
			for(int i=1; i < numarCozi; i++){
				int lung = coada[ i ].lungimeCoada();
				if (lung < min){
					min = lung;
					index = i;
				}
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
		return index;
	}

	@Override
	public void run(){
		try{
			int i=0;
			int m;
			sleep(1000);
			ClientiGUI.text += "Ora de start: " + ClientiGUI.dataSDF + "\n";
			while( i<numarClienti ){
				i++;
				Client client = new Client( ++ID, ClientiGUI.timpSosire, ClientiGUI.timpProcesare);
				m = min_index();
				sleep( client.getTimpSosire() * 1000);
				coada[m].adaugaClient(client);
				AVRG[m] = ((x[m]-1)*AVRG[m]+client.getTimpSosire()+client.getTimpProcesare()) / x[m];
				x[m]++;
				ClientiGUI.text += (ClientiGUI.dataSDF + " Clientul " + Integer.toString(client.getID())+" a fost adaugat la coada "+ Integer.toString(m));
				ClientiGUI.text += (" Sosire " + Integer.toString(client.getTimpSosire()) + " Procesare " + Integer.toString(client.getTimpProcesare()) + "\n");
				medieAsteptare = String.format("%.3f", AVRG[0]) +"s       " + String.format("%.3f", AVRG[1]) +"s       " + String.format("%.3f", AVRG[2])+"s";
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
	}
}
